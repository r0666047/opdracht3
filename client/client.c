#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <fcntl.h>
#include <sys/sendfile.h>

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void SendFile (char *File,int sockfd)
{
	struct stat file_stat;
	char fileSize[256];
	ssize_t len;
	int offset;
	int remaingData;
	int sentBytes = 0;
	int fd;
	int n;
	char buffer[256];
	
	fd = open(File, O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "Error opening file\r\n");
        exit(EXIT_FAILURE);
    }
	n = write(sockfd,File,18);

    bzero(buffer,256);
    n = read(sockfd,buffer,255);    

    if (fstat(fd, &file_stat) < 0)
    {
            fprintf(stderr, "Error fstat\r\n");

            exit(EXIT_FAILURE);
    }

    fprintf(stdout, "File Size: \n%d bytes\n", file_stat.st_size);

    sprintf(fileSize, "%d", file_stat.st_size);

    len = send(sockfd, fileSize, sizeof(fileSize), 0);
    if (len < 0)
    {
            fprintf(stderr, "Error on sending\r\n");
            exit(EXIT_FAILURE);
    }

    offset = 0;
    remaingData = file_stat.st_size;

    while (((sentBytes = sendfile(sockfd, fd, &offset, BUFSIZ)) > 0) && (remaingData > 0))
    {
            remaingData -= sentBytes;
    }
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];
	char File[256];
	
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port (file to send)\n", argv[0]);
       exit(0);
    }
	
	
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
	
	SendFile(argv[3],sockfd);
	
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
    close(sockfd);
    return 0;
}
